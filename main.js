


document.getElementById("create").addEventListener("click", function(){    
    let name = document.getElementById("data_name").value;
    let power = document.getElementById("data_power").value;
    let id = Heroes[Heroes.length - 1].id + 1;
    
    let data = {id, name, power};    

    createData(Heroes, data, (err, result)=>{        
        if(err){
            alert(err);
        }
        else
        {
            console.log(result);
            $("#results").css("display", "none");
            document.getElementById("name").setAttribute("type", "hidden");
            document.getElementById("poder").setAttribute("type", "hidden");
        }
    });
});
document.getElementById("read").addEventListener("click", function(){
    
    let id = parseInt(document.getElementById("id").value);
   
    readData(Heroes, id, (err, result)=>{
        if(err){
            alert(err);
        }else{            
            $("#results").css("display", "none");
            document.getElementById("name").value = result.name;
            document.getElementById("poder").value = result.power;
            document.getElementById("name").removeAttribute("type");
            document.getElementById("poder").removeAttribute("type");                
        }
    });  
});
document.getElementById("update").addEventListener("click", function(){
    let name = document.getElementById("data_name").value;
    let power = document.getElementById("data_power").value;
    let id = document.getElementById("id").value;

    let data = {id, name, power}; 
    document.getElementById("name").setAttribute("type", "hidden");
    document.getElementById("poder").setAttribute("type", "hidden"); 

    updateData(Heroes, id, data, (err, result)=>{
        
        if(err){
            alert(err);
        }else{
            $("#results").css("display", "none");
            console.log(result);
        }

    })    
});
document.getElementById("delete").addEventListener("click", function(){
    
    let name = document.getElementById("data_name").value;
    let power = document.getElementById("data_power").value;
    let id = parseInt(document.getElementById("id").value);

    let data = {id, name, power};

    deleteData(Heroes, id, (err, result)=>{
        if(err){
            alert(err);
        }else{
            console.log(result);
            $("#results").css("display", "none");
            document.getElementById("name").setAttribute("type", "hidden");
            document.getElementById("poder").setAttribute("type", "hidden"); 
        }

    })    
});

