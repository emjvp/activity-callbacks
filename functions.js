

let createData = (array, data, callback)=>{//it is a function defined to create a value
    let startLenghtArray = array.length//it is the lenght now, at present
     //it add a new element to the arrays
    if(data.id != undefined && data.name != undefined && data.power != undefined){        
        array.push(data);    
        if (startLenghtArray === array.length)//compare the initial lenght
        {            
            callback("you had a problem with the insertion", null);//error, response
        }
        else
        {            
            callback(null, "your insertion was succesfull");//response, error
        }
    }
    else
    {
        callback("you had a problem with the insertion", null); //response, error
    }    
};
let readData=(array, id, callback)=>{//it is a function that allow read an especific value
    let i = 0;//it is a control variable
    let dataFound = undefined;//it is the "object" that recive the values founded
    while(i<array.length && dataFound == undefined){//the declaration of a while
        if(array[i].id === id){//if the id of the array is equal to the id wanted, the condition is achieved
            dataFound={//the definition of the "object"
                id:array[i].id,//the id
                name:array[i].name,//the name asigned
                power:array[i].power//the power
            };
        }
        i++;//the increment of the control variable
    }   
    
    if(dataFound == undefined){//if dataFound is undefined there was a problem
        callback("the parameter is wrong",null);//return a function with the error response
    }else{
        callback(null,dataFound);//return the function with the dataFound filled        
    }
};
let updateData = (array, id, data, callback)=>{//update an array position
    
    let i;//a control variable
    
    for(i = 0; i < array.length; i++)// a bucle for to go across the array
    {
        if(array[i].id == id){//if the id of the array is equal to the id passed on the function, the condition is achieved
            array[i] = {//it is for replace the values in the position that was matched
                id: parseInt(id),
                name: data.name,
                power: data.power
            };            
            break;
        }
    }
    if(array[i] != undefined){
        callback(null, array[i]);
    }else{
        callback("the values has not been updated", null);
    }
};
let deleteData = (array, id, callback)=>{
    let i = 0;
    let found = undefined;

    do{
        if(array[i].id == id){   
            found = {
                id: array[i].id,
                name: array[i].name,
                power: array[i].power
            };
            array.splice(i, 1);            
        }
        i++;
    }while(i < array.length && found == undefined);
    if(found){
        callback(null, found);
        
    }else{        
        callback("the value has not been deleted", null);
    }
};


